# charts (DEPRECATED)

This repo is deprecated.

The code has been moved to these repos:

- https://gitlab.com/devopscoop/charts
- https://codeberg.org/devopscoop/charts
- https://github.com/devopscoop/charts
